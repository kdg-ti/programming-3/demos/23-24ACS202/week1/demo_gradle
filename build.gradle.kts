plugins {
    id("java")
    id("application")
}

application {
    mainClass.set("be.kdg.programming3.StartApplication")
}


group = "be.kdg.programming3"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    implementation("com.afrunt.randomjoke:random-joke-crawler:0.2.2")
}

tasks.test {
    useJUnitPlatform()
}